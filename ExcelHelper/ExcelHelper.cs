using ExcelExtension.Util;

namespace ExcelExtension
{
    /// <summary>
    /// 
    /// </summary>
    public class ExcelHelper
    {
        /// <summary>
        /// Single Instance
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static IExcel GetInstance(string fileName)
        {
            var excel = fileName.EndsWith(".xls") ? new Excel2003(fileName) : (IExcel)new Excel2007(fileName);
            return excel;
        }
    }
}