using System.Linq;
using NPOI.SS.UserModel;

namespace ExcelExtension.Utils
{
    /// <summary>
    /// 
    /// </summary>
    public static class RowExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="columnIndex"></param>
        /// <returns></returns>
        public static ICell GetCell1(this IRow row, int columnIndex)
        {
            return row.Cells.FirstOrDefault(c => c.ColumnIndex == columnIndex);
        }
    }
}