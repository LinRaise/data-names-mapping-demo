using System.IO;
using NPOI.HSSF.UserModel;

namespace ExcelExtension.Util
{
    internal class Excel2003 : ExcelBase
    {
        public Excel2003(string fileName) : base(fileName)
        {

        }
        protected override void InitWorkbook(FileStream fs)
        {
            workbook = fs != null ? new HSSFWorkbook(fs) : new HSSFWorkbook();
        }
    }
}