using System.IO;
using NPOI.XSSF.UserModel;

namespace ExcelExtension.Util
{
    internal class Excel2007 : ExcelBase
    {
        public Excel2007(string fileName) : base(fileName)
        {
        }
        protected override void InitWorkbook(FileStream fs)
        {
            workbook = fs != null ? new XSSFWorkbook(fs) : new XSSFWorkbook();
        }
    }
}