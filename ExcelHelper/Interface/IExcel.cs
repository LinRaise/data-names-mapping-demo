using System.Collections.Generic;
using System.Data;

namespace ExcelExtension.Util
{
    /// <summary>
    /// 
    /// </summary>
    public interface IExcel
    {
        /// <summary>
        /// 
        /// </summary>
        string FileName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<string> GetSheets();
        void InitWorkbook();
        void SaveToExcel();
        int WriteDataTable(DataTable dataTable, string sheetName, bool isContainCaption, int firstRowNum = 0, int firstColNum = 0);
        void WriteDataTable(DataTable dt, string workSheetName, bool isContainCaption, string[] fieldsToCompare, string statusColumn, string keyColumn, string summaryPrefix = "", string reviewField = null, int firstRowNum = 0, int firstColNum = 0);
        //int WriteDataTable(DataTable dataTable, NPOI.SS.UserModel.ISheet sheet, bool isContainCaption, int firstRowNum = 0, int firstColNum = 0);
        int WriteDataTableToExcel(DataTable dataTable, string sheetName, bool isContainCaption, int firstRowNum = 0, int firstColNum = 0);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="isFirstRowCaption"></param>
        /// <returns></returns>
        DataTable ReadDataTable(string sheetName, bool isFirstRowCaption);
        DataTable ReadDataTable(string sheetName, bool isFirstRowCaption, int firstRowNum = 0, int firstColNum = 0);
        DataTable ReadDataTable(string sheetName, string[] headers, int firstRowNum = 0, int firstColNum = 0);

    }
}