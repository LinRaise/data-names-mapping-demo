using Npoi.Mapper.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace DataTableMapperTests.Contracts
{
    public class Demo1
    {
        [Column(0)]
        public string LEDGER_CODE { get; set; }

        [Column("Ledger Name")]
        public string LEDGER_NAME { get; set; }

        [Column("Instrument Code")]
        public string INSTRUMENT_CODE { get; set; }

        [Column("Currency Code")]
        public string CURRENCY_CODE { get; set; }

        [Column("ISIN Future")]
        public string ISIN_FUTURE { get; set; }

        [Column("Maturity date - From Name")]
        public string MATURITY_DATE_FROM_NAME { get; set; }

        [Column("Instrument Long Name")]
        public string INSTRUMENT_LONG_NAME { get; set; }

        [Column("Volume")]
        public string VOLUME { get; set; }

        [Column("Market Value")]
        public string MARKET_VALUE { get; set; }

        [Column("Market Rate")]
        public string MARKET_RATE { get; set; }

        [Column("Percentage for Valuation")]
        public string PERCENTAGE_FOR_VALUATION { get; set; }
        [Column("PREFIX")]
        public string PREFIX { get; set; }

    }
}