﻿using DataTableMapperTests.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Npoi.Mapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Npoi.Mapper.Tests
{
    [TestClass()]
    public class MapperTests
    {
        [TestMethod()]
        public void MapperTest()
        {
            var currentDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var demo1path = Path.Combine(currentDirectory, "ExampleData", "demo1.xlsx");

            var mapper = new Mapper(demo1path);
            var trade1 = mapper.Take<Demo1>().ToList();
            Assert.IsNotNull(trade1);
        }

        [TestMethod()]
        public void MapperTest2()
        {
            var currentDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var demo2path = Path.Combine(currentDirectory, "ExampleData", "demo2.xlsx");

            var mapper = new Mapper(demo2path);
            var trade2 = mapper.Take<dynamic>().ToList();
            Assert.IsNotNull(trade2);
        }
    }
}