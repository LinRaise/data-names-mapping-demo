using DataTableMapperTests.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataTableMapperTests.Mapping
{
    public static class AttributeHelper
    {
        public static DataNamesAttribute GetDataNameAttr(PropertyInfo prop)
        {
            var property = prop.GetCustomAttribute<DataNamesAttribute>(false);
            //var property = prop.GetCustomAttributes(false).Where(x => x.GetType().Name == nameof(DataNamesAttribute)).FirstOrDefault();

            if (property != null)
            {
                return property;
            }
            return new DataNamesAttribute();
        }
    }
}