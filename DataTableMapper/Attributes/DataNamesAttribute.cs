using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTableMapperTests.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DataNamesAttribute : Attribute
    {
        protected IEnumerable<string> _valueNames { get; set; }
        protected int _index { get; set; } = -1;
        protected string _customFormat = string.Empty;

        public IEnumerable<string> ValueNames
        {
            get
            {
                return _valueNames;
            }
            set
            {
                _valueNames = value;
            }
        }

        public int Index
        {
            get
            {
                return _index;
            }
            set
            {
                _index = value;
            }
        }
        public string CustomFormat
        {
            get
            {
                return _customFormat;
            }
            set
            {
                _customFormat = value;
            }
        }
        public DataNamesAttribute()
        {
            _valueNames = new List<string>();
            _index = -1;
            _customFormat = string.Empty;
        }

        public DataNamesAttribute(params string[] valueNames) : this(-1, valueNames) { }
        
        public DataNamesAttribute(int index, params string[] valueNames) : this(string.Empty, index, valueNames) { }

        public DataNamesAttribute(string customformat, int index, params string[] valueNames)
        {
            _valueNames = valueNames.ToList();
            _index = index;
            _customFormat = customformat;
        }
    }
}