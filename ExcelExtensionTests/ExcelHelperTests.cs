﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExcelExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Npoi.Mapper;

namespace ExcelExtension.Tests
{
    [TestClass()]
    public class ExcelHelperTests
    {
        [TestMethod()]
        public void ExcelExtensionTests()
        {
            var currentDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var demo1path = Path.Combine(currentDirectory, "ExampleData", "demo1.xlsx");
            var demo2path = Path.Combine(currentDirectory, "ExampleData", "demo1.xls");

            var data1 = ExcelHelper.GetInstance(demo1path).ReadDataTable("Sheet1", true);
            var data2 = ExcelHelper.GetInstance(demo2path).ReadDataTable("Sheet1", true);

        }

        [TestMethod]
        public void ReadLargeExcel()
        {
            string path1 = @"C:\DevinLin\SourceCode\Demo\DataNamesMappingDemo\TestData\excel\demo1.xlsx";
            string path2 = @"C:\DevinLin\SourceCode\Demo\DataNamesMappingDemo\TestData\excel\demo2.xlsx";
            string path3 = @"C:\DevinLin\SourceCode\Demo\DataNamesMappingDemo\TestData\excel\demo3.xlsx";

#if false
            var data1 = ExcelHelper.GetInstance(path1).ReadDataTable("demo1", true);
            Assert.IsNotNull(data1);
#endif

#if false
            var data2 = ExcelHelper.GetInstance(path2).ReadDataTable("demo2", true);
            Assert.IsNotNull(data2);
#endif

#if true
            var data3 = ExcelHelper.GetInstance(path3).ReadDataTable("data3", true);
            Assert.IsNotNull(data3);
#endif
        }
    }
}