﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelXmlConverter.Utils
{
    public static class ExcelUtil
    {
        public static void DataSetToExcel(DataSet dataSet , string filePath)
        {
            using(ExcelPackage pck = new ExcelPackage())
            {
                foreach (DataTable table in dataSet.Tables)
                {
                    ExcelWorksheet workSheet = pck.Workbook.Worksheets.Add(table.TableName);
                    workSheet.Cells["A1"].LoadFromDataTable(table, true);
                }
                pck.SaveAs(new FileInfo(filePath));
            }
        }
    }
}
