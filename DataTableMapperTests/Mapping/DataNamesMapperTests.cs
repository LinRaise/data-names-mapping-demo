﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using DataTableMapperTests.DataSets;
using DataTableMapperTests.Contracts;
using System.IO;
using ExcelExtension;

namespace DataTableMapperTests.Mapping.Tests
{
    [TestClass()]
    public class DataNamesMapperTests
    {
        [TestMethod()]
        public void MapTest()
        {
            var priestsDataSet = DataSetGenerator.GenDataSet1();
            List<Person> persons = DataNamesMapper<Person>.Instance.Map(priestsDataSet.Tables[0]).ToList();
            
            var ranchersDataSet = DataSetGenerator.GenDataSet2();
            persons.AddRange(DataNamesMapper<Person>.Instance.Map(ranchersDataSet.Tables[0]));


            var currentDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var demo1xlsx = Path.Combine(currentDirectory, "ExampleData", "demo1.xlsx");
            var demo1xls = Path.Combine(currentDirectory, "ExampleData", "demo1.xls");

            var data1 = ExcelHelper.GetInstance(demo1xlsx).ReadDataTable("Sheet1", true);
            var data2 = ExcelHelper.GetInstance(demo1xls).ReadDataTable("Sheet1", true);

            var demo1 = DataNamesMapper<Demo1>.Instance.Map(data1);
            var demo2 = DataNamesMapper<Demo1>.Instance.Map(data2);

            //persons.AddRange(demo1);
            //persons.AddRange(demo2);

            foreach (var person in persons)
            {
                Console.WriteLine("First Name: " + person.FirstName + ", Last Name: " + person.LastName
                                  + ", Date of Birth: " + person.DateOfBirth.ToShortDateString()
                                  + ", Job Title: " + person.JobTitle + ", Nickname: " + person.TakenName
                                  + ", Is American: " + person.IsAmerican);
            }

            
        }
    }
}