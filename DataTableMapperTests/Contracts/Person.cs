using DataTableMapperTests.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTableMapperTests.Contracts
{
    public class Person
    {
        [DataNames(0, " ", "firstName")]
        public string FirstName { get; set; }

        [DataNames(1,"last_name", "lastName")]
        public string LastName { get; set; }

        [DataNames(2,"dob", "dateOfBirth")]
        public DateTime DateOfBirth { get; set; }

        [DataNames(3,"job_title", "jobTitle")]
        public string JobTitle { get; set; }

        [DataNames(4,"taken_name", "nickName")]
        public string TakenName { get; set; }
        
        [DataNames(5,"is_american", "isAmerican")]
        public bool IsAmerican { get; set; }
    }
}