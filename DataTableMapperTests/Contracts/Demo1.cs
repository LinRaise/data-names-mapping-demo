﻿using DataTableMapperTests.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTableMapperTests.Contracts
{
    class Demo1
    {
        [DataNames(0)]
        public string Ledger_Code { get; set; }

        [DataNames("Ledger Name")]
        public string Ledger_Name { get; set; }

        [DataNames("Instrument Code")]
        public string Instrument_Code { get; set; }

        [DataNames("Currency Code")]
        public string Currency_Code { get; set; }

        [DataNames("ISIN Future")]
        public string ISIN_Future { get; set; }

        [DataNames("Maturity date - From Name")]
        public string Maturity_date_From_Name { get; set; }

        [DataNames("Instrument Long Name")]
        public string Instrument_Long_Name { get; set; }

        [DataNames("Volume")]
        public string Volume { get; set; }

        [DataNames("Market Value")]
        public string Market_Value { get; set; }

        [DataNames("Market Rate")]
        public string Market_Rate { get; set; }

        [DataNames("Percentage for Valuation")]
        public string Percentage_for_Valuation { get; set; }

        [DataNames(11)]
        public string PREFIX { get; set; }

    }
}
