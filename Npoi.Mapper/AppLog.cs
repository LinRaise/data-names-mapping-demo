﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Npoi.Mapper
{
    public class AppLog
    {
        private static readonly ILogger _logger = LogManager.GetLogger(_logfile);
        private const string _logfile = "File";

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static AppLog() { }
        private AppLog() { }

        public static void Info(object msg)
        {
            _logger.Info(msg);
        }
    }
}
