﻿using Npoi.MapperConsoleDemo.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Npoi.MapperConsoleDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var currentDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var demo1path = Path.Combine(currentDirectory, "ExampleData", "demo1.xlsx");

            var mapper = new Npoi.Mapper.Mapper(demo1path);
            var trade1 = mapper.Take<Demo1>();
        }
    }
}
